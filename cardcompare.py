def readfromcsv(filename, merchantcol, cardcol, numChars, delimiter=","):
    outputArray=[]
    if (numChars==0):
        with open(filename, "r") as f:
            for line in f:
                splitline = line.split(delimiter)
                cardnum = splitline[cardcol]
                firstWordMerchant = splitline[merchantcol].replace('\n', '').replace('\r', '')
                outputArray.append([cardnum, firstWordMerchant])
    else:
        with open(filename, "r") as f:
            for line in f:
                splitline = line.split(delimiter)
                cardnum = splitline[cardcol]
                firstWordMerchant = splitline[merchantcol][:numChars]
                outputArray.append([cardnum, firstWordMerchant])
    return outputArray
def makeunique(inputArray, cardpos=0, merchantpos=1):
    searchArray = []
    for row in inputArray:
        try:
            # search for card and merchant in checked
            searchArray.index([row[cardpos], row[merchantpos]])
        except:
            # if the search fails, write it to checked
            searchArray.append([row[cardpos], row[merchantpos]])
    return searchArray
def countcards(inputArray, cardcol=0):
    cardcount = 1
    PreCard = inputArray[0][cardcol]
    for row in inputArray:
        if (row[cardcol] != PreCard):
            cardcount = cardcount+1
            PreCard = row[cardcol]
    return cardcount
def countmerchantoccurances(inputArray, merchantcol=1):
    counter = {}
    for row in inputArray:
        try:
            updated = counter[row[merchantcol]]+1
            counter.update({row[merchantcol]: updated})
        except:
            counter.update({row[merchantcol]: 1})
    return counter
