# Credit Card Merchant Matching
***Made for Alliant Credit Union***

### What is this for
This is for matching up credit card history, across multiple cards to find what matches across the cards. This is useful in determining whether fraudulent card transactions are a result of an internal breach, or external breach. If it is an external breach, it will return what merchant all or most of the cards where used at. 

### How to use
#### Functions:
* readfromcsv(filename, merchantcol, cardcol, numChars, delimiter=",")
  Reads raw card data CSV file into array.
  * **filename:** The name of the CSV file.
  * **cardcol:** The column of the credit card number in the CSV file.
  * **merchantcol:** The column of the merchant in the CSV file.
  * **numChars:** The number of characters to use from the beginning of the merchant name. If set to 0, it will take the whole merchant column.


* makeunique(inputArray, cardpos=0, merchantpos=1)
  Makes every Merchant unique to the card using it. For example: if there are multiple Jewel Osco charges on one card, it will strip every one after the first.
  * **inputArray:** The array with the raw card data. Usually set with *readfromcsv.*
  * **cardpos:** Defaults to 0, but can be set manually. It is the index position of the credit card number in the *inputArray.* It shouldn't need to be used if *readfromcsv* is set properly.
  * **merchantpos:** Defaults to 1, but can be set manually. It is the index position of the merchant name in the *inputArray.* It shouldn't need to be used if *readfromcsv* is set properly.


* countcards(inputArray, cardcol=0)
  Counts the number of cards loaded.
  * **inputArray:** The array with the unique card data. Usually set with *makeunique.*
  * **cardcol:** Defaults to 0, but can be set manually. It is the index position of the credit card number in the *inputArray.* It shouldn't need to be used if *readfromcsv* is set properly.


* countmerchantoccurances(inputArray, merchantcol=1)
  Counts the number of times a merchant appears in the *unique list*
  * **inputArray:** The array with the unique card data. Usually set with *makeunique.*
  * **merchantcol:** Defaults to 1, but can be set manually. It is the index position of the merchant name in the *inputArray.* It shouldn't need to be used if *readfromcsv* is set properly.
