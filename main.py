import operator
from cardcompare import *
## Variables ##
# File to Analyse
filename = "carddata/carddata.csv"
#CSV Delimiter
delimiter = ","
# Card Collumn
cardcol = 0
# Merchant Collumn
merchantcol = 1
# Number of Characters returned from the Merchant name
# Good for when you suspect the merchant to have a short name (this might create look-alike matches, but not always the case)
# Uses the whole merchant name if set to 0
numChars = 0
#Number of hits returned
outputcount = 15

## Program ##
# Read cards and (first 6 digits of) merchants into array "csv"
csv = readfromcsv(filename=filename, merchantcol=merchantcol, cardcol=cardcol, numChars=numChars)

# Make each card and merchant unique
checked = makeunique(inputArray=csv)

# counts the number of unique cards
cardcount = countcards(inputArray=checked)

# Counts the number of occurances for each merchant
counter = countmerchantoccurances(inputArray=checked)

# Print results
print "Analysis Proramming by: Charlie Melidosian"
print "Returning the first " + str(numChars) + " characters of merchant only."
i=0
while (i<outputcount):
    merchantname = sorted(counter, reverse=True ,key=counter.__getitem__)[i]
    print merchantname + " - " + str(counter[merchantname]) + "/" + str(cardcount)
    i = i+1
